package roubek

import (
	hex2 "encoding/hex"
)

type hexEnc struct{}

func (d hexEnc) Decode(in []byte) (interface{}, error) {
	if len(in) >= 2 && in[0] == '0' && in[1] == 'x' {
		in = in[2:]
	}

	out := make([]byte, hex2.DecodedLen(len(in)))
	n, err := hex2.Decode(out, in)
	if err != nil {
		return nil, err
	}
	return out[:n], nil
}

func init() {
	RegisterDecoder("hex", hexEnc{})
}
