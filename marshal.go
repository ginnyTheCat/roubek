package roubek

import (
	"encoding/json"
	"fmt"

	"github.com/clbanning/mxj/v2"
)

type UnmarshalDecoder func([]byte, interface{}) error

func (d UnmarshalDecoder) Decode(in []byte) (interface{}, error) {
	var v interface{}
	err := d(in, &v)
	if err != nil {
		return nil, err
	}
	return v, nil
}

func init() {
	jsonDec := UnmarshalDecoder(json.Unmarshal)
	RegisterDecoder("json", jsonDec)

	RegisterDecoder("json-string", DecodeFunc(func(in []byte) (interface{}, error) {
		return jsonDec.Decode([]byte(fmt.Sprintf(`"%s"`, in)))
	}))

	RegisterDecoder("xml", DecodeFunc(func(in []byte) (interface{}, error) {
		m, err := mxj.NewMapXml(in)
		if err != nil {
			return nil, err
		}
		return map[string]interface{}(m), nil
	}))
}
