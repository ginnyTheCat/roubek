package roubek

import (
	"bytes"
	"compress/gzip"
	"io"
)

func init() {
	RegisterDecoder("gzip", DecodeFunc(func(in []byte) (interface{}, error) {
		r, err := gzip.NewReader(bytes.NewReader(in))
		if err != nil {
			return nil, err
		}
		return io.ReadAll(r)
	}))
}
