package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"reflect"
	"sort"
	"strings"

	"lelux.net/roubek"
)

func main() {
	err := run()
	if err != nil {
		log.Fatal(err)
	}
}

func addFlags(f *flag.FlagSet) (*string, *string) {
	using := f.String("using", "", "use a specific decoder")
	path := f.String("path", "", "use a path to access nested objects")

	return using, path
}

func run() error {
	listDecoders := flag.Bool("list-decoders", false, "list all decoders")
	raw := flag.Bool("raw", false, "output the raw binary data")
	script := flag.String("script", "", "launch a decode script")
	using, path := addFlags(flag.CommandLine)

	flag.Parse()

	if *listDecoders {
		decs := roubek.ListDecoders()
		sort.Strings(decs)
		fmt.Println(strings.Join(decs, "\n"))
		return nil
	}

	var b []byte
	var err error
	if flag.NArg() > 0 {
		b = []byte(strings.Join(flag.Args(), " "))
	} else {
		b, err = io.ReadAll(os.Stdin)
		if err != nil {
			return err
		}
	}

	if *script == "" {
		b, err = line(b, *using, *path, *raw)
		if err != nil {
			return err
		}
		os.Stdout.Write(b)
	} else {
		data, err := os.ReadFile(*script)
		if err != nil {
			return err
		}

		lines := strings.Split(string(data), "\n")

		for i, l := range lines {
			f := flag.NewFlagSet("script", flag.ContinueOnError)

			usage, path := addFlags(f)

			err = f.Parse(strings.Split(l, " "))
			if err != nil {
				return err
			}

			b, err = line(b, *usage, *path, *raw || i != len(lines)-1)
			if err != nil {
				return err
			}
		}

		os.Stdout.Write(b)
	}

	return nil
}

func line(b []byte, using string, path string, raw bool) ([]byte, error) {
	if using == "" {
		decC := make(chan roubek.DecodeRes)
		go roubek.Decode(b, decC)

		var err error
		for dec := range decC {
			dec.Data, err = roubek.Path(dec.Data, path)
			if err != nil {
				return nil, err
			}

			data := roubek.Bytes(dec.Data)
			if data != nil {
				dec.Data = data
			}

			if dec.Name != "raw" && reflect.DeepEqual(dec.Data, b) {
				continue
			}

			fmt.Println(dec)
		}
	} else {
		dec, err := roubek.DecodeUsing(using, b)
		if err != nil {
			return nil, err
		}

		dec.Data, err = roubek.Path(dec.Data, path)
		if err != nil {
			return nil, err
		}

		if raw {
			return roubek.Raw(dec.Data)
		} else {
			data := roubek.Bytes(dec.Data)
			if data != nil {
				dec.Data = data
			}

			fmt.Println(dec)
		}
	}

	return nil, nil
}
