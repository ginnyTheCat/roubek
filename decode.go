package roubek

import (
	"encoding/hex"
	"encoding/json"
	"fmt"
	"strconv"
	"strings"
)

type Decoder interface {
	Decode(in []byte) (out interface{}, err error)
}

type DecodeFunc func(in []byte) (interface{}, error)

func (d DecodeFunc) Decode(in []byte) (interface{}, error) {
	return d(in)
}

type DecodeRes struct {
	Name string
	Data interface{}
}

func (r DecodeRes) String() string {
	var b strings.Builder

	headerLen := 78
	b.Grow(headerLen + 1)

	padLen := headerLen - len(r.Name) - 2
	padHalf := padLen / 2

	b.WriteString(strings.Repeat("=", padHalf))
	b.WriteRune(' ')
	b.WriteString(r.Name)
	b.WriteRune(' ')
	b.WriteString(strings.Repeat("=", padLen-padHalf))
	b.WriteRune('\n')

	truncateLen := 16 * 20

	switch data := r.Data.(type) {
	case []byte:
		if len(data) > truncateLen {
			b.WriteString(hex.Dump(data[:truncateLen]))
			b.WriteString("\n[truncated] len=")
			b.WriteString(strconv.Itoa(len(data)))
			b.WriteRune('\n')
		} else {
			b.WriteString(hex.Dump(data))
		}
		return b.String()
	case interface{}:
		j, err := json.MarshalIndent(data, "", "  ")
		if err == nil {
			b.Write(j)
			b.WriteRune('\n')
			return b.String()
		}
	}

	dataStr := fmt.Sprint(r.Data)

	if len(dataStr) > truncateLen {
		b.WriteString(dataStr[:truncateLen])
		b.WriteString("\n\n[truncated] len=")
		b.WriteString(strconv.Itoa(len(dataStr)))
	} else {
		b.WriteString(dataStr)
	}

	b.WriteRune('\n')

	return b.String()
}

var decoders = make(map[string]Decoder)

func RegisterDecoder(name string, d Decoder) {
	decoders[name] = d
}

func ListDecoders() []string {
	keys := make([]string, 0, len(decoders))
	for key := range decoders {
		keys = append(keys, key)
	}
	return keys
}

func Decode(b []byte, c chan<- DecodeRes) {
	for name, dec := range decoders {
		d, err := dec.Decode(b)
		if err == nil {
			c <- DecodeRes{
				Name: name,
				Data: d,
			}
		}
	}
	close(c)
}

func DecodeUsing(name string, b []byte) (DecodeRes, error) {
	dec := decoders[name]
	d, err := dec.Decode(b)
	if err != nil {
		return DecodeRes{}, err
	}

	return DecodeRes{
		Name: name,
		Data: d,
	}, nil
}
