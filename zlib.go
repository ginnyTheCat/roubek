package roubek

import (
	"bytes"
	"compress/zlib"
	"io"
)

func init() {
	RegisterDecoder("zlib", DecodeFunc(func(in []byte) (interface{}, error) {
		r, err := zlib.NewReader(bytes.NewReader(in))
		if err != nil {
			return nil, err
		}
		defer r.Close()

		return io.ReadAll(r)
	}))
}
