package roubek

import (
	"bytes"
	"compress/bzip2"
	"io"
)

func init() {
	RegisterDecoder("bzip2", DecodeFunc(func(in []byte) (interface{}, error) {
		r := bzip2.NewReader(bytes.NewReader(in))
		return io.ReadAll(r)
	}))
}
