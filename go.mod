module lelux.net/roubek

go 1.16

require (
	github.com/clbanning/mxj/v2 v2.5.5
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
)
