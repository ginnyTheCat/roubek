package roubek

import "net/url"

func init() {
	RegisterDecoder("url", DecodeFunc(func(in []byte) (interface{}, error) {
		s, err := url.PathUnescape(string(in))
		if err != nil {
			return nil, err
		}
		return string(s), nil
	}))
}
