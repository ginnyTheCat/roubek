package roubek

import (
	"github.com/dgrijalva/jwt-go"
)

type jwtDec struct {
	parser jwt.Parser
}

type jwtData struct {
	Method jwt.SigningMethod
	Header map[string]interface{}
	jwt.Claims
	Signature []byte
}

func (d jwtDec) Decode(in []byte) (interface{}, error) {
	t, parts, err := d.parser.ParseUnverified(string(in), jwt.MapClaims{})
	if err != nil {
		return nil, err
	}

	return jwtData{
		Method:    t.Method,
		Header:    t.Header,
		Claims:    t.Claims,
		Signature: []byte(parts[2]),
	}, nil
}

func init() {
	RegisterDecoder("jwt", jwtDec{})
}
