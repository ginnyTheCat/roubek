package roubek

import "encoding/json"

func Bytes(i interface{}) []byte {
	switch t := i.(type) {
	case []byte:
		return t
	case byte:
		return []byte{t}
	case string:
		return []byte(t)
	default:
		return nil
	}
}

func Raw(i interface{}) ([]byte, error) {
	b := Bytes(i)
	if b != nil {
		return b, nil
	}
	return json.Marshal(i)
}
