package roubek

import (
	"reflect"
	"strconv"
	"strings"
)

func Path(value interface{}, path string) (interface{}, error) {
	if path == "" {
		return value, nil
	}

	v := reflect.ValueOf(value)

	paths := strings.Split(path, ".")
	for _, part := range paths {
		if v.Kind() == reflect.Interface {
			v = v.Elem()
		}

		switch v.Kind() {
		case reflect.Map:
			v = v.MapIndex(reflect.ValueOf(part))
		case reflect.Struct:
			v = v.FieldByName(part)
		case reflect.Slice, reflect.String:
			i, err := strconv.Atoi(part)
			if err != nil {
				return nil, err
			}
			if i < 0 {
				i = v.Len() + i
			}
			v = v.Index(i)
		}
	}

	return v.Interface(), nil
}
