package roubek

import (
	"html"
)

func init() {
	RegisterDecoder("html", DecodeFunc(func(in []byte) (interface{}, error) {
		return []byte(html.UnescapeString(string(in))), nil
	}))
}
