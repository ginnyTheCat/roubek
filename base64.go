package roubek

import "encoding/base64"

type base64enc struct {
	*base64.Encoding
}

func (d base64enc) Decode(in []byte) (interface{}, error) {
	out := make([]byte, d.DecodedLen(len(in)))
	n, err := d.Encoding.Decode(out, in)
	if err != nil {
		return nil, err
	}
	return out[:n], nil
}

func init() {
	RegisterDecoder("base64", base64enc{base64.StdEncoding})
	RegisterDecoder("base64-raw", base64enc{base64.RawStdEncoding})
	RegisterDecoder("base64-url", base64enc{base64.URLEncoding})
	RegisterDecoder("base64-raw-url", base64enc{base64.RawURLEncoding})
}
