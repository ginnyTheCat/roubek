package roubek

func init() {
	RegisterDecoder("raw", DecodeFunc(func(in []byte) (interface{}, error) {
		return in, nil
	}))
}
